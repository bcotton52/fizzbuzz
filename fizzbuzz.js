// Write a function named fizzBuzz that takes one parameter: maxValue.
function fizzBuzz(maxValue) {
    let x = 0;
    let string = '';
    for (let i = 0; i < maxValue; i++) {
        x++;
        if (x % 2 === 0 && x % 3 === 0) {
            string += 'FizzBuzz, ';
        } else if (x % 2 === 0) {
            string += 'Fizz, ';
        } else if (x % 3 === 0) {
            string += 'Buzz, ';
        } else {
            string += x + ', ';
        }       
    }
    return string; 
}

console.log(fizzBuzz(100))
// This function should loop through 1 - maxValue (inclusive).
// If a number is even and a multiple of 3 concatenate "FizzBuzz, " to the end of your string.
// Bonus
// Write a second function named fizzBuzzPrime.
// This function will follow all of the rules of your fizzBuzz function and 1 additional rule:
// If a number is a prime number, concatenate "Prime, " to the end of your string